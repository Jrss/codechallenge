(function() {
    'use strict';

    angular
        .module('codechallengeApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
