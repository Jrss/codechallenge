(function () {
    'use strict';

    angular
        .module('codechallengeApp')
        .controller('HomeController', HomeController);

    function HomeController() {
        var vm = this;
        vm.run = run;
        vm.stop = stop;
        vm.updateMatrix = updateMatrix;
        vm.matrix = [];

        //create a 50x50 matrix
        for (let row = 0; row < 50; row++) {
            vm.matrix[row] = Array.apply(null, Array(50)).map(function (x, i) { 
                //populate the matrix with random 0s and 1s.
                return (parseInt(Math.random()*100))%2; 
            });
        }

        drawMatrix(vm.matrix);

        function drawMatrix(grid) {
            var c = document.getElementById("myCanvas");
            var ctx = c.getContext("2d");
            ctx.clearRect(0, 0, 500, 500);

            grid.forEach(function (row, rowNumber) {
                grid.forEach(function (column, columnNumber) {
                    ctx.beginPath();
                    ctx.lineWidth = "1";
                    ctx.strokeStyle = "blue";
                    //if the value of the cell is 1, we fill the square with blue.
                    if (grid[rowNumber][columnNumber] == 1) {
                        ctx.fillStyle = "blue";
                        ctx.fillRect(10 * rowNumber, 10 * columnNumber, 10, 10);
                    } else {
                        //if the cell's value is 0, we draw a white square with blue border
                        ctx.rect(10 * rowNumber, 10 * columnNumber, 10, 10);
                        ctx.fillStyle = "white";
                        ctx.fill();
                        ctx.lineWidth = 1;
                    }
                    ctx.stroke();
                });
            });
        }


        //This function executes the logic of the game
        function updateMatrix() {
            //I create another 50x50 matrix, where I will store the values after each calculation.
            var matrixCopy = [];
            for (let row = 0; row < 50; row++) {
                matrixCopy[row] = Array.apply(null, Array(50)).map(function (x, i) { return 0; });
            }
            for (var row= 1; row< 49; row++) {
                for (var column = 1; column < 49; column++) {
                    var totalCells = 0;
                    //add up the total values of surrounding cells
                    totalCells += vm.matrix[row- 1][column - 1];
                    totalCells += vm.matrix[row- 1][column];
                    totalCells += vm.matrix[row- 1][column + 1];
                    totalCells += vm.matrix[row][column - 1]; 
                    totalCells += vm.matrix[row][column + 1];
                    totalCells += vm.matrix[row+ 1][column - 1];
                    totalCells += vm.matrix[row+ 1][column];
                    totalCells += vm.matrix[row+ 1][column + 1];

                    //Apply the logic:
                    //If cell is dead
                    if (vm.matrix[row][column] === 0) {
                        switch (totalCells) {
                            //If a dead cell has exactly three live neighbours, it comes to life
                            case 3:
                                matrixCopy[row][column] = 1;
                                break;
                            //otherwise, leave it dead.
                            default:
                                matrixCopy[row][column] = 0; //otherwise leave it dead
                        }
                    //If the cell is alive
                    } else if (vm.matrix[row][column] === 1) {
                        switch (totalCells) {
                            //If a live cell has less than two live neighbours, it dies
                            case 0:
                            case 1:
                                matrixCopy[row][column] = 0;
                                break;
                            //If a live cell has two or three live neighbours, it continues living
                            case 2:
                            case 3:
                                matrixCopy[row][column] = 1; 
                                break;
                            //If a live cell has more than three live neighbours, it dies
                            case 4:
                            case 5:
                            case 6:
                            case 7:
                            case 8:
                                matrixCopy[row][column] = 0;
                                break;
                            default:
                                matrixCopy[row][column] = 0;
                        }
                    }
                }
            }
            //Copy the result to the matrix we're processing to show in the canvas
            vm.matrix = angular.copy(matrixCopy);
            drawMatrix(vm.matrix);
        }

        //function called when the user hits "run"
        function run(){
            vm.t = setInterval(updateMatrix,1000);
        }
        
        //function called when the user hits "stop"
        function stop(){
            clearInterval(vm.t);
            vm.t = undefined;
        }
    }
})();
