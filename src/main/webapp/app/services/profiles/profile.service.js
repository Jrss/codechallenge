(function() {
    'use strict';

    angular
        .module('codechallengeApp')
        .factory('ProfileService', ProfileService);

    function ProfileService() {

        var service = {
            getProfileInfo : getProfileInfo
        };

        return service;

        function getProfileInfo() {
            var response = {};
            response.activeProfiles = 'dev';
            response.ribbonEnv = 'dev';
            response.inProduction = false;
            response.swaggerEnabled = false;
            return response;;
        }
    }
})();
