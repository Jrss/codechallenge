## Frontend Code Challenge - for Vivid Seats ##

This is a frontend assessment, where I developed the logic of the Conway's Game of Life.

The project is developed using AngularJS and HTML5 as main technologies. It also uses Bower, NPM, SASS, 
Bootstrap, Gulp and some other tools and frameworks that helped me generate the project fast and easily.

The project was generated using the tool JHipster for a faster solution (running the command: jhipster client).

TO RUN THE APP: 
    - Open a console and enter the home folder of the project (codechallenge)
    - First run: gulp build
    - Then run the command: gulp serve

Author: Juliana Rossi.
Contact:
    - Email: juliana.rossi@hotmail.com.ar or julianarossi.2@gmail.com
    - Skype Id: julianarossi2